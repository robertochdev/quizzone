//
//  ViewController.swift
//  Quizzone
//
//  Created by Roberto Chiarella on 03/05/18.
//  Copyright © 2018 Roberto Chiarella. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var gameImageView: UIImageView!
    
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var feedbackLabel: UILabel!
    
    
    @IBOutlet var firstButton: UIButton!
    
    @IBOutlet var secondButton: UIButton!
    
    var questionsArray: [Question] = []
    var currentQuestion:Question?
    var points : Int = 0
    var index : Int = 0
    var gameEnded: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstButton.layer.cornerRadius = 10
        self.secondButton.layer.cornerRadius = 10
        self.setQuestions()
        self.currentQuestion = questionsArray[index]
        
        self.setGUI()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func answerButton_clicked(_ sender: UIButton) {
        
        guard let title = sender.titleLabel?.text
            else{
                return
        }
        
        guard let question = self.currentQuestion else{
            return
        }
        
        if title == question.answerCorrect{
            if self.gameEnded == false{
                self.points += 1
                
                print("Risposta esatta il tuo punteggio è \(self.points)/\(self.questionsArray.count)")
            }
        }else{
            if self.gameEnded == false{
            print("Risposta sbagliata")
            }
        }
        
        
        if self.index >= self.questionsArray.count-1{
            print("Gioco finito")
            self.gameEnded = true
            
            self.feedbackLabel.isHidden = false
            self.feedbackLabel.text = "Punteggio: \(self.points)/\(self.questionsArray.count)"
            
            return
        }
        
        
        self.index += 1
        self.currentQuestion =  self.questionsArray[index]
        self.setGUI()
        
    }
    func setGUI(){
        
        guard  let question = self.currentQuestion else {
            return
        }
        
        self.gameImageView.image = question.image
        self.questionLabel.text = question.text
        self.firstButton.setTitle(question.answerCorrect, for: .normal)
        self.secondButton.setTitle(question.answerWrong, for: .normal)
    }
    
    func setQuestions(){
        self.questionsArray = [Question.init(text: "Quale è il tipo di pianta?",
                                             image: #imageLiteral(resourceName: "DroseracapensisHeader.jpg"),
                                             answerCorrect: "pianta carnivora",
                                             answerWrong: "pianta grassa"),
                               Question.init(text: "Quale è il nome della pianta?",
                                             image: #imageLiteral(resourceName: "img_0-335016_sarracenia_flava_rubricorpora_gd.jpg"),
                                             answerCorrect: "Sarracenia",
                                             answerWrong: "Drosera"),
                               Question.init(text: "La nepenthes può arrivare a misurare 20 cm",
                                             image: #imageLiteral(resourceName: "NepenthesHeader.png"),
                                             answerCorrect: "Falso",
                                             answerWrong: "Vero")
        ]
    }

    
    
    
}

