//
//  Question.swift
//  Quizzone
//
//  Created by Roberto Chiarella on 03/05/18.
//  Copyright © 2018 Roberto Chiarella. All rights reserved.
//

import UIKit

struct Question{
    let text : String
    let image : UIImage
    let answerCorrect : String
    let answerWrong : String
    
}


